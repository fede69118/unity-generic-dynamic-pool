﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool<T> : MonoBehaviour where T : MonoBehaviour
{
    public T objectToPool;
    public int initialObjects = 10;
    public int poolExpandLimit = 10;

    public List<T> freeObjects;
    public List<T> usedObjects;

    private void Awake()
    {
        InitiatePool();
    }

    void InitiatePool()
    {
        freeObjects = new List<T>();
        usedObjects = new List<T>();

        for (int i = 0; i < initialObjects; i++)
        {
            var newObject = GameObject.Instantiate(objectToPool, transform);
            newObject.gameObject.SetActive(false);
            newObject.transform.position = Vector3.zero;
            newObject.transform.eulerAngles = Vector3.zero;

            freeObjects.Add(newObject);
        }
    }

    void ExpandPool()
    {
        for (int i = 0; i < poolExpandLimit; i++)
        {
            var newObject = GameObject.Instantiate(objectToPool, transform);
            newObject.gameObject.SetActive(false);
            newObject.transform.position = Vector3.zero;
            newObject.transform.eulerAngles = Vector3.zero;

            freeObjects.Add(newObject);
        }
    }

    public virtual T Get()
    {
        var freeNum = freeObjects.Count;
        if (freeNum == 0)
        {
            ExpandPool();
            freeNum = freeObjects.Count;
        }

        var obj = freeObjects[freeNum - 1];
        freeObjects.RemoveAt(freeNum - 1);
        usedObjects.Add(obj);
        

        return obj;
    }

    public virtual void ReturnObject(T _pooledObject)
    {
        if (usedObjects.Count == 0)
        {
            Debug.LogWarning("No such objects are being used.");
            return;
        }

        // Put the pooled object back in the free list.
        usedObjects.Remove(_pooledObject);
        freeObjects.Add(_pooledObject);

        // Reparent the pooled object to us, and disable it.
        var pooledObjectTransform = _pooledObject.transform;
        //pooledObjectTransform.parent = transform;
        pooledObjectTransform.localPosition = Vector3.zero;
        _pooledObject.gameObject.SetActive(false);
    }
}
